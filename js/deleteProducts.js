$(document).ready(function(){
    $('#product_delete_form').submit(function() {
      const data = $(this).serializeArray();
      if (data.length < 2) {
          alert("No items selected for deletion.");
          return false;
      }
      $.ajax({
          type: $(this).attr('method'),
          url: $(this).attr('action'),
          data: data,
          success: function(data) {
              $('.mass_delete_chkbox:checked').parents('.product_box').remove();
              $('.mass_delete_chkbox').prop('checked', false);
              alert(data.message);
          },
      });
      return false;
    });
});
