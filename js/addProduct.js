$(document).ready(function(){
  //show appropriate attribute based on type
    $('#type_select').change(function() {
    const type_id = $(this).val();

    //hide previous selected type attribute
    $('.active_type').addClass('d-none');
    $('input.active_type').prop('required', false);
    $('.active_type').removeClass('active_type');

    switch(type_id) {
      case 'DvdDiscs':
        $('.type_dvd').prop('required', true);
        $('.type_dvd').removeClass('d-none');
        $('.type_dvd').addClass('active_type');
        break;
      case 'Books':
        $('.type_book').prop('required', true);
        $('.type_book').removeClass('d-none');
        $('.type_book').addClass('active_type');
        break;
      case 'Furniture':
          $('.type_furniture').prop('required', true);
          $('.type_furniture').removeClass('d-none');
          $('.type_furniture').addClass('active_type');
          break;
    }
    });

    $('#product_add_form').submit(function() {

    $.ajax({
      type: $(this).attr('method'),
      url: $(this).attr('action'),
      data: $(this).serializeArray(),
      success: function(data) {
          console.log(data);
          alert(data.message);
      },
    });
    return false;
});

});
