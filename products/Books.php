<?php
require_once("Products.php");

class Books extends Products
{
    private $weight;

    /*
    * set object variables with single book item data
    * @param $sku unique product identifier
    * @param $name name of the product
    * @param $price price of the product
    * @param $attribs array of product type specific attributes
    * @return none
    */
    public function __construct(string $sku, string $name, float $price, $attribs)
    {
        parent::__construct($sku, $name, $price);
        $this->weight = intval($attribs[4]);
    }

    /*
    * Set current object weight property to given integer
    * @param $weight int book weight in kg
    * @return none
    */
    public function setWeight(int $weight)
    {
        $this->weight = $weight;
    }

    /*
    * Return book object properties as an associative array
    * @return $properties associatve array of book item attributes
    */
    public function getProperties()
    {
        $properties = parent::getProperties();
        $properties["specialAttrib"] = "Weight: $this->weight KG";
        return $properties;
    }

    /*
    * Save product data stored in this object to database
    * @return none
    */
    public function addProduct()
    {
        $this->conn->begin_transaction();
        $productSql = "INSERT INTO products.products (sku, name, price) VALUES (?, ?, ?)";
        $discSql = "INSERT INTO products.books (sku, weight) VALUES (?, ?)";

        $stmt = $this->conn->prepare($productSql);
        $stmt->bind_param("ssd", $this->sku, $this->name, $this->price);
        $stmt->execute();

        $stmt = $this->conn->prepare($discSql);
        $stmt->bind_param("si", $this->sku, $this->weight);
        $stmt->execute();

        if ($this->conn->commit() === true) {
            Helper::returnStatus(1, "Product data successfuly saved.");
        } else {
            Helper::returnStatus(0, $this->conn->error);
        }
    }

    /*
    * Get all book data stored in database
    * @return $objs array of Books objects
    */
    public static function getAllBooks()
    {
        $conn = Helper::setConn();
        $query = <<<SQL
        SELECT books.sku, books.weight, products.price, products.name
        FROM products.books
        JOIN products.products on products.sku = books.sku;
SQL;
        $result = mysqli_query($conn, $query);
        while($data = $result->fetch_object()) {
            $obj = new Books($data->sku, $data->name, $data->price, null);
            $obj->setWeight($data->weight);
            $objs[] = $obj;
        }
        
        return (isset($objs)) ? $objs : array();
    }
}
