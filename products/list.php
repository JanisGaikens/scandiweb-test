<?php
require_once("base.html");
require_once("Products.php");
require_once("DvdDiscs.php");
require_once("Books.php");
require_once("Furniture.php");

$books = Books::getAllBooks();
$dvddiscs = DvdDiscs::getAllDiscs();
$furniture = Furniture::getAllFurniture();
$products = array_merge($dvddiscs, $books, $furniture);
?>
<head>
    <title>Product list</title>
    <script src="../js/deleteProducts.js"></script>
</head>
<body>
    <form action="delete.php" method="post" id="product_delete_form">
        <header>
          <h1 class="view_title">Product List</h1>
          <input class="submit_btn" type="submit" value="Apply"/>
          <select name="list_action" id="list_action_select" required>
            <option disabled selected value>Actions switcher</option>
            <option value="mass_delete">Mass Delete Action</option>
          </select>
        </header>
        <div class="product_list">
          <?php foreach($products as $product): ?>
            <?php $attribs = $product->getProperties(); ?>
            <div class="product_box">
              <input class="mass_delete_chkbox" type="checkbox" name="delete_sku[]" value="<?php echo $attribs["sku"];?>">
              <p><?php echo $attribs["sku"];?></p>
              <p><?php echo $attribs["name"];?></p>
              <p><?php echo $attribs["price"];?> $</p>
              <p><?php echo $attribs["specialAttrib"];?></p>
            </div>
          <?php endforeach; ?>
        </div>
    </form>
</body>
