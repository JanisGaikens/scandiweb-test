<?php
require_once("Products.php");
require_once("Furniture.php");
require_once("Books.php");
require_once("DvdDiscs.php");

try {
    $new_product = new $_POST["type"](
        $_POST["sku"],
        $_POST["name"],
        floatval($_POST["price"]),
        $_POST["special_attrib"]
    );
} catch (TypeError $error) {
    $new_product->returnStatus(0, "input types invalid");
    exit;
}
$new_product->addProduct();
exit;
