<?php
require_once("Products.php");

class DvdDiscs extends products
{
    private $size;

    /*
    * set object variables with single dvd disc item data
    * @param $sku unique product identifier
    * @param $name name of the product
    * @param $price price of the product
    * @param $attribs array of product type specific attributes
    * @return none
    */
    public function __construct(string $sku, string $name, float $price, $attribs)
    {
        parent::__construct($sku, $name, $price);
        $this->size = intval($attribs[0]);
    }

    /*
    * Set current object size property to given integer
    * @param $weight int DVD disc size in MB
    * @return none
    */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /*
    * Return furniture object properties as an associative array
    * @return $properties associatve array of furniture item attributes
    */
    public function getProperties()
    {
        $properties = parent::getProperties();
        $properties["specialAttrib"] = "Size: $this->size MB";
        return $properties;
    }

    /*
    * Save product data stored in this object to database
    * @return none
    */
    public function addProduct()
    {
        $this->conn->begin_transaction();
        $productSql = "INSERT INTO products.products (sku, name, price) VALUES (?, ?, ?)";
        $discSql = "INSERT INTO products.dvd_discs (sku, size) VALUES (?, ?)";

        $stmt = $this->conn->prepare($productSql);
        $stmt->bind_param("ssd", $this->sku, $this->name, $this->price);
        $stmt->execute();

        $stmt = $this->conn->prepare($discSql);
        $stmt->bind_param("si", $this->sku, $this->size);
        $stmt->execute();

        if ($this->conn->commit() === true) {
            Helper::returnStatus(1, "Product data successfuly saved.");
        } else {
            Helper::returnStatus(0, $this->conn->error);
        }
    }

    /*
    * Get all DVD disc data stored in database
    * @return $objs array of DvdDiscs objects
    */
    public static function getAllDiscs()
    {
        $conn = Helper::setConn();
        $query = <<<SQL
        SELECT dvd_discs.sku, dvd_discs.size, products.price, products.name
        FROM products.dvd_discs
        JOIN products.products on products.sku = dvd_discs.sku;
SQL;
        $result = mysqli_query($conn, $query);
        while($data = $result->fetch_object()) {
            $obj = new DvdDiscs($data->sku, $data->name, $data->price, null);
            $obj->setSize($data->size);
            $objs[] = $obj;
        }

        return (isset($objs)) ? $objs : array();
    }

}
