<?php
require_once("Products.php");

class Furniture extends products
{
    private $height;
    private $length;
    private $width;

    /*
    * set object variables with single furniture item data
    * @param $sku unique product identifier
    * @param $name name of the product
    * @param $price price of the product
    * @param $attribs array of product type specific attributes
    * @return none
    */
    public function __construct(string $sku, string $name, float $price, $attribs)
    {
        parent::__construct($sku, $name, $price);
        $this->height = intval($attribs[1]);
        $this->width = intval($attribs[2]);
        $this->length = intval($attribs[3]);
    }

    /*
    * Set current object dimension properties to given integer
    * @param $height int furniture height in cm
    * @param $width int furniture width in cm
    * @param $length int furniture length in cm
    * @return none
    */
    public function setDimensions(int $height, int $width, int $length)
    {
        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
    }

    /*
    * Return furniture object properties as an associative array
    * @return $properties associatve array of furniture item attributes
    */
    public function getProperties()
    {
        $properties = parent::getProperties();
        $properties["specialAttrib"] = "Dimension: $this->height" . "x" .
        $this->width . "x" . $this->length;

        return $properties;
    }

    /*
    * Save product data stored in this object to database
    * @return none
    */
    public function addProduct()
    {
        $this->conn->begin_transaction();
        $productSql = "INSERT INTO products.products (sku, name, price) VALUES (?, ?, ?)";
        $furnitureSql = "INSERT INTO products.furniture (sku, width, height, length)".
        "VALUES (?, ?, ?, ?)";

        $stmt = $this->conn->prepare($productSql);
        $stmt->bind_param("ssd", $this->sku, $this->name, $this->price);
        $stmt->execute();

        $stmt = $this->conn->prepare($furnitureSql);
        $stmt->bind_param("siii", $this->sku, $this->width, $this->height, $this->length);
        $stmt->execute();

        if ($this->conn->commit() === true) {
            Helper::returnStatus(1, "Product data successfuly saved.");
        } else {
            Helper::returnStatus(0, $this->conn->error);
        }
    }

    /*
    * Get all furniture data stored in database
    * @return $objs array of Furniture objects
    */
    public static function getAllFurniture()
    {
        $conn = Helper::setConn();
        $query = <<<SQL
        SELECT furniture.sku, furniture.height, furniture.width, furniture.length,
        products.price, products.name
        FROM products.furniture
        JOIN products.products on products.sku = furniture.sku;
SQL;
        $result = mysqli_query($conn, $query);
        while($data = $result->fetch_object()) {
            $obj = new Furniture($data->sku, $data->name, $data->price, null);
            $obj->setDimensions($data->height, $data->width, $data->length);
            $objs[] = $obj;
        }
        
        return (isset($objs)) ? $objs : array();
    }
}
