<?php
require_once("base.html");
?>
<head>
    <title>Product add</title>
    <script src="../js/addProduct.js"></script>
</head>
<body>
    <form action="save.php" method="post" id="product_add_form">
        <header>
          <h1 class="view_title">Product Add</h1>
          <input class="submit_btn" type="submit" value="Save"/>
        </header>
        <div class="form_body">
          <label for="sku" class="label_align">SKU</label>
          <input name="sku" type="text" maxlength="255" required />
          <br />

          <label for="name" class="label_align">Name</label>
          <input name="name" type="text" maxlength="255" required />
          <br />

          <label for="price" class="label_align">Price</label>
          <input name="price" type="text"  required />
          <br />

          <label for="type" class="label_align">Type Switcher</label>
          <select name="type" id="type_select" required>
            <option disabled selected value>Type switcher</option>
            <option value="DvdDiscs">DVD-disc</option>
            <option value="Books">Book</option>
            <option value="Furniture">Furniture</option>
          </select>
          <br />

          <label for="special_attrib[]" class="label_align type_dvd d-none">Size</label>
          <input name="special_attrib[]" class="type_dvd d-none" type="text" />
          <p class="type_dvd attribute_desc d-none">
            Please provide size of the DVD-disc in megabytes
          </p>

          <label for="special_attrib[]" class="label_align type_furniture d-none">Height</label>
          <input name="special_attrib[]" class="type_furniture d-none" type="text" />
          <br />

          <label for="special_attrib[]" class="label_align type_furniture d-none">Width</label>
          <input name="special_attrib[]" class="type_furniture d-none" type="text" />
          <br />

          <label for="special_attrib[]" class="label_align type_furniture d-none">Length</label>
          <input name="special_attrib[]" class="type_furniture d-none" type="text" />
          <p class="type_furniture attribute_desc d-none">
            Please provide dimensions using HxWxL format in centimeters of the furniture
          </p>

          <label for="special_attrib[]" class="label_align type_book d-none">Weight</label>
          <input name="special_attrib[]" class="type_book d-none" type="text" />
          <p class="type_book attribute_desc d-none">
            Please provide weight of the book in kilograms
          </p>
        </div>
    </form>
</body>
