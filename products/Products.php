<?php

require_once("Helper.php");

class Products
{
    protected $sku;
    protected $name;
    protected $price;
    protected $conn;

    /*
    * set object variables with single product data
    * @param $sku unique product identifier
    * @param $name name of the product
    * @param $price price of the product
    * @return none
    */
    public function __construct(string $sku, string $name, float $price)
    {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->conn = Helper::setConn();
    }

    /*
    * Return Products object properties in associative array
    * @return $properties associative array of Products object properties
    */
    public function getProperties()
    {
        $properties = array(
            "sku" => $this->sku,
            "name" => $this->name,
            "price" => $this->price
        );
        return $properties;
    }

    /*
    * Delete specified products from database
    * @param $skuArr array of unique product identifiers
    * @return none
    */
    public static function deleteProducts($skuArr)
    {
        $conn = Helper::setConn();
        $skuArr = array_map(array($conn, "real_escape_string"), $skuArr);
        $in = implode('", "', $skuArr);
        $sql = <<<SQL
        DELETE FROM products.books WHERE sku IN ("$in");
        DELETE FROM products.dvd_discs WHERE sku IN ("$in");
        DELETE FROM products.furniture WHERE sku IN ("$in");
        DELETE FROM products.products WHERE sku IN ("$in");
SQL;
        if ($conn->multi_query($sql)=== true) {
            Helper::returnStatus(1, "Selected items successfuly deleted.");
        } else {
            Helper::returnStatus(0, $conn->error);
        }
    }
}
