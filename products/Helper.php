<?php

class Helper
{
    /*
    * Helper function to connect to mysql database using mysqli
    * @return mysqli object
    */
    public static function setConn()
    {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $conn = new mysqli($servername, $username, $password);
        if ($conn->connect_error) {
            Helper::returnStatus(0, $this->conn->connect_error);
        }
        return $conn;
    }

    /*
    * Helper function to return status and message to AJAX calls
    * @param $status status code 0 - error, 1 - success
    * @param $message message to return to AJAX call
    * @return json encoded array of status and message provided to the method.
    */
    public static function returnStatus($status, $message)
    {
        header("Content-Type: application/json");
        echo json_encode(array("status" => $status, "message" => $message));
        exit;
    }
}
